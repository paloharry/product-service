package com.example.demo.product;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table
public class Product {
    @Id
    @SequenceGenerator(
            name= "product_sequence",
            sequenceName = "product_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "product_sequence"
    )
    private Long id;
    private String title;
    private String imageURL;
    private Long rangeId;
    private Long seasonId;
    private Long sourceTypeId;
    private String year;
    //    private Long c4Number;
    private Float netWeight;
    private Float netContent;
    private Float fscWeight;
    private Float cartonNetWeight;
    private Float cartonGrossWeight;
    private String hsCode;
    private Integer cartonsPerPallet;
    private Integer pcsPerCarton;
    private Integer dimensionsId;
    private Integer packedDimensionsId;
    private Integer cartonDimensionsId;
    private Integer productUnitId;
    private String originalSupplierId;
    private String factoryReferenceNo;
    private String materialRemarks;
    private String packagingRemarks;
    private String specRemarks;
    private Date createdAt;
    private Date createdBy;
    private Date updatedAt;
    private Date updatedBy;

    public Product() {
    }

    public Product(Long id,
                   String title,
                   String imageURL,
                   Long rangeId,
                   Long seasonId,
                   Long sourceTypeId,
                   String year,
                   Float netWeight,
                   Float netContent,
                   Float fscWeight,
                   Float cartonNetWeight,
                   Float cartonGrossWeight,
                   String hsCode,
                   Integer cartonsPerPallet,
                   Integer pcsPerCarton,
                   Integer dimensionsId,
                   Integer packedDimensionsId,
                   Integer cartonDimensionsId,
                   Integer productUnitId,
                   String originalSupplierId,
                   String factoryReferenceNo,
                   String materialRemarks,
                   String packagingRemarks,
                   String specRemarks,
                   Date createdAt,
                   Date createdBy,
                   Date updatedAt,
                   Date updatedBy) {
        this.id = id;
        this.title = title;
        this.imageURL = imageURL;
        this.rangeId = rangeId;
        this.seasonId = seasonId;
        this.sourceTypeId = sourceTypeId;
        this.year = year;
        this.netWeight = netWeight;
        this.netContent = netContent;
        this.fscWeight = fscWeight;
        this.cartonNetWeight = cartonNetWeight;
        this.cartonGrossWeight = cartonGrossWeight;
        this.hsCode = hsCode;
        this.cartonsPerPallet = cartonsPerPallet;
        this.pcsPerCarton = pcsPerCarton;
        this.dimensionsId = dimensionsId;
        this.packedDimensionsId = packedDimensionsId;
        this.cartonDimensionsId = cartonDimensionsId;
        this.productUnitId = productUnitId;
        this.originalSupplierId = originalSupplierId;
        this.factoryReferenceNo = factoryReferenceNo;
        this.materialRemarks = materialRemarks;
        this.packagingRemarks = packagingRemarks;
        this.specRemarks = specRemarks;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }

    public Product(String title,
                   String imageURL,
                   Long rangeId,
                   Long seasonId,
                   Long sourceTypeId,
                   String year,
                   Float netWeight,
                   Float netContent,
                   Float fscWeight,
                   Float cartonNetWeight,
                   Float cartonGrossWeight,
                   String hsCode,
                   Integer cartonsPerPallet,
                   Integer pcsPerCarton,
                   Integer dimensionsId,
                   Integer packedDimensionsId,
                   Integer cartonDimensionsId,
                   Integer productUnitId,
                   String originalSupplierId,
                   String factoryReferenceNo,
                   String materialRemarks,
                   String packagingRemarks,
                   String specRemarks,
                   Date createdAt,
                   Date createdBy,
                   Date updatedAt,
                   Date updatedBy) {
        this.title = title;
        this.imageURL = imageURL;
        this.rangeId = rangeId;
        this.seasonId = seasonId;
        this.sourceTypeId = sourceTypeId;
        this.year = year;
        this.netWeight = netWeight;
        this.netContent = netContent;
        this.fscWeight = fscWeight;
        this.cartonNetWeight = cartonNetWeight;
        this.cartonGrossWeight = cartonGrossWeight;
        this.hsCode = hsCode;
        this.cartonsPerPallet = cartonsPerPallet;
        this.pcsPerCarton = pcsPerCarton;
        this.dimensionsId = dimensionsId;
        this.packedDimensionsId = packedDimensionsId;
        this.cartonDimensionsId = cartonDimensionsId;
        this.productUnitId = productUnitId;
        this.originalSupplierId = originalSupplierId;
        this.factoryReferenceNo = factoryReferenceNo;
        this.materialRemarks = materialRemarks;
        this.packagingRemarks = packagingRemarks;
        this.specRemarks = specRemarks;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Long getRangeId() {
        return rangeId;
    }

    public void setRangeId(Long rangeId) {
        this.rangeId = rangeId;
    }

    public Long getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Long seasonId) {
        this.seasonId = seasonId;
    }

    public Long getSourceTypeId() {
        return sourceTypeId;
    }

    public void setSourceTypeId(Long sourceTypeId) {
        this.sourceTypeId = sourceTypeId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Float getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Float netWeight) {
        this.netWeight = netWeight;
    }

    public Float getNetContent() {
        return netContent;
    }

    public void setNetContent(Float netContent) {
        this.netContent = netContent;
    }

    public Float getFscWeight() {
        return fscWeight;
    }

    public void setFscWeight(Float fscWeight) {
        this.fscWeight = fscWeight;
    }

    public Float getCartonNetWeight() {
        return cartonNetWeight;
    }

    public void setCartonNetWeight(Float cartonNetWeight) {
        this.cartonNetWeight = cartonNetWeight;
    }

    public Float getCartonGrossWeight() {
        return cartonGrossWeight;
    }

    public void setCartonGrossWeight(Float cartonGrossWeight) {
        this.cartonGrossWeight = cartonGrossWeight;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public Integer getCartonsPerPallet() {
        return cartonsPerPallet;
    }

    public void setCartonsPerPallet(Integer cartonsPerPallet) {
        this.cartonsPerPallet = cartonsPerPallet;
    }

    public Integer getPcsPerCarton() {
        return pcsPerCarton;
    }

    public void setPcsPerCarton(Integer pcsPerCarton) {
        this.pcsPerCarton = pcsPerCarton;
    }

    public Integer getDimensionsId() {
        return dimensionsId;
    }

    public void setDimensionsId(Integer dimensionsId) {
        this.dimensionsId = dimensionsId;
    }

    public Integer getPackedDimensionsId() {
        return packedDimensionsId;
    }

    public void setPackedDimensionsId(Integer packedDimensionsId) {
        this.packedDimensionsId = packedDimensionsId;
    }

    public Integer getCartonDimensionsId() {
        return cartonDimensionsId;
    }

    public void setCartonDimensionsId(Integer cartonDimensionsId) {
        this.cartonDimensionsId = cartonDimensionsId;
    }

    public Integer getProductUnitId() {
        return productUnitId;
    }

    public void setProductUnitId(Integer productUnitId) {
        this.productUnitId = productUnitId;
    }

    public String getOriginalSupplierId() {
        return originalSupplierId;
    }

    public void setOriginalSupplierId(String originalSupplierId) {
        this.originalSupplierId = originalSupplierId;
    }

    public String getFactoryReferenceNo() {
        return factoryReferenceNo;
    }

    public void setFactoryReferenceNo(String factoryReferenceNo) {
        this.factoryReferenceNo = factoryReferenceNo;
    }

    public String getMaterialRemarks() {
        return materialRemarks;
    }

    public void setMaterialRemarks(String materialRemarks) {
        this.materialRemarks = materialRemarks;
    }

    public String getPackagingRemarks() {
        return packagingRemarks;
    }

    public void setPackagingRemarks(String packagingRemarks) {
        this.packagingRemarks = packagingRemarks;
    }

    public String getSpecRemarks() {
        return specRemarks;
    }

    public void setSpecRemarks(String specRemarks) {
        this.specRemarks = specRemarks;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Date createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Date updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", rangeId=" + rangeId +
                ", seasonId=" + seasonId +
                ", sourceTypeId=" + sourceTypeId +
                ", year='" + year + '\'' +
                ", netWeight=" + netWeight +
                ", netContent=" + netContent +
                ", fscWeight=" + fscWeight +
                ", cartonNetWeight=" + cartonNetWeight +
                ", cartonGrossWeight=" + cartonGrossWeight +
                ", hsCode='" + hsCode + '\'' +
                ", cartonsPerPallet=" + cartonsPerPallet +
                ", pcsPerCarton=" + pcsPerCarton +
                ", dimensionsId=" + dimensionsId +
                ", packedDimensionsId=" + packedDimensionsId +
                ", cartonDimensionsId=" + cartonDimensionsId +
                ", productUnitId=" + productUnitId +
                ", originalSupplierId='" + originalSupplierId + '\'' +
                ", factoryReferenceNo='" + factoryReferenceNo + '\'' +
                ", materialRemarks='" + materialRemarks + '\'' +
                ", packagingRemarks='" + packagingRemarks + '\'' +
                ", specRemarks='" + specRemarks + '\'' +
                ", createdAt=" + createdAt +
                ", createdBy=" + createdBy +
                ", updatedAt=" + updatedAt +
                ", updatedBy=" + updatedBy +
                '}';
    }
}


