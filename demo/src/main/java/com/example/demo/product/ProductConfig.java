package com.example.demo.product;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;
import java.util.List;

@Configuration
public class ProductConfig {

    @Bean
    CommandLineRunner commandLineRunner(ProductRepository repository) {
        return args -> {
            Product airPod = new Product(
                    1L,
                    "AirPod",
                    "/api/test/url",
                    1L,
                    1L,
                    1L,
                    "2021/12",
                    1.2f,
                    1.3f,
                    1.4f,
                    1.5f,
                    1.6f,
                    "74331231",
                    10,
                    11,
                    13,
                    1,
                    1,
                    1,
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    new Date(),
                    new Date(),
                    new Date(),
                    new Date()
            );

            repository.saveAll(
                    List.of(airPod)
            );
        };
    }
}
